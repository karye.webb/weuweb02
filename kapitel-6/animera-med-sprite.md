# Animera med sprite

![tank-sprite.png](<../.gitbook/assets/image (2).png>)

{% tabs %}
{% tab title="tank.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Tilesheet</title>
    <link rel="stylesheet" href="./tank.css">
</head>
<body>
    <div class="kontainer">
        <canvas></canvas>
    </div>
    <script>
        // Element vi arbetar med
        const eCanvas = document.querySelector("canvas");

        // Ställ in bredd och storlek
        eCanvas.width = 800;
        eCanvas.height = 600;

        // Starta canvas rityta
        var ctx = eCanvas.getContext("2d");

        var tileSheet = new Image();
        tileSheet.src = "./tank-sprite.png";

        var tankRutor = [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6];
        var index = 0;

        function ritaTank() {
            var ruta = tankRutor[index] * 32;
            ctx.drawImage(tileSheet, ruta, 0, 32, 32, 250, 50, 50, 50);

            index++;
            if (index == tankRutor.length) {
                index = 0;
            }
        }

        // Animationsloopen
        function gameLoop() {
            // Rensa canvas
            ctx.clearRect(0, 0, eCanvas.width, eCanvas.height);

            ritaTank();

            requestAnimationFrame(gameLoop);
        }

        // Starta spelet
        gameLoop();

        // https://www.oreilly.com/library/view/html5-canvas/9781449308032/ch04.html
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="tank.css" %}
```css
body {
    background: #F9F6EB;
}
.kontainer {
    width: 900px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
}
.kol2 {
    display: grid;
    grid-template-columns: auto auto;
    gap: 1em;
}
canvas {
    background: #FFF;
    border: 1px solid #CCC;
    width: 700px;
}
button {
    margin: 3em 0;
}
```
{% endtab %}
{% endtabs %}
