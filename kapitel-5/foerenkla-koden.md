# Förenkla koden

## En generell funktion

* Vi har 5 funktioner som är exakt likadana

```javascript
// Rita ut pidgey
function ritaPidgey() {
..
}
// Rita ut psyduck
function ritaPsyduck() {
..
}
// Rita ut desura
function ritaDesura() {
..
}
// Rita ut eevee
function ritaEevee() {
..
}
// Rita ut meowth
function ritaMeowth() {
..
}
```

* Vi skapar en mer generell funktion
* Som vi kan mata objekten med

```javascript
function ritaMonster(figur) {
    figur.y += 3;
    if (figur.y > 1000) {
        figur.y = 0;
        figur.x = Math.random() * 1000;
    }
    ctx.drawImage(figur.bild, figur.x, figur.y);
}
```

## I animationsloopen

```javascript
// Animationsloopen
function loopen() {
    // Sudda ut hela canvas
    ctx.clearRect(0, 0, 1200, 1000);

    // Rita ut pikachu
    ritaPikachu();

    // Rita ut monstren
    ritaMonster(pidgey);
    ritaMonster(psyduck);
    ritaMonster(desura);
    ritaMonster(eevee);
    ritaMonster(meowth);
    ritaMonster(mankey);
    ritaMonster(dratini);
    ritaMonster(piggies);
    ritaMonster(crash);

    requestAnimationFrame(loopen);
}
```
