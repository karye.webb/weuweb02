# Infoga fler figurer

```javascript
/* ****************************** */
/*           Objekten             */
/* ****************************** */
var pidgey = {
    x: Math.random() * 1000,
    y: -Math.random() * 500,
    bild: new Image()
};
var psyduck = {
    bild: new Image(),
    x: Math.random() * 1000,
    y: -Math.random() * 500
};
var desura = {
    bild: new Image(),
    x: Math.random() * 1000,
    y: -Math.random() * 500
};
var eevee = {
    bild: new Image(),
    x: Math.random() * 1000,
    y: -Math.random() * 500
};
var meowth = {
    bild: new Image(),
    x: Math.random() * 1000,
    y: -Math.random() * 500
};

// Ladda in bilderna
pidgey.bild.src = "./icon8/icons8-pidgey-50.png";
psyduck.bild.src = "./icon8/icons8-psyduck-50.png";
desura.bild.src = "./icon8/icons8-desura-50.png";
eevee.bild.src = "./icon8/icons8-eevee-50.png";
meowth.bild.src = "./icon8/icons8-meowth-50.png";

/* ****************************** */
/*     Starta animationsloopen    */
/* ****************************** */
loopen();

/* ****************************** */
/*            Funktioner          */
/* ****************************** */

// Rita ut pidgey
function ritaPidgey() {
    pidgey.y++;
    if (pidgey.y > 1000) {
        pidgey.y = 0;
        pidgey.x = Math.random() * 1000;
    }
    ctx.drawImage(pidgey.bild, pidgey.x, pidgey.y);
}
// Rita ut psyduck
function ritaPsyduck() {
    psyduck.y++;
    if (psyduck.y > 1000) {
        psyduck.y = 0;
        psyduck.x = Math.random() * 1000;
    }
    ctx.drawImage(psyduck.bild, psyduck.x, psyduck.y);
}
// Rita ut desura
function ritaDesura() {
    desura.y++;
    if (desura.y > 1000) {
        desura.y = 0;
        desura.x = Math.random() * 1000;
    }
    ctx.drawImage(desura.bild, desura.x, desura.y);
}
// Rita ut eevee
function ritaEevee() {
    eevee.y++;
    if (eevee.y > 1000) {
        eevee.y = 0;
        eevee.x = Math.random() * 1000;
    }
    ctx.drawImage(eevee.bild, eevee.x, eevee.y);
}
// Rita ut meowth
function ritaMeowth() {
    meowth.y++;
    if (meowth.y > 1000) {
        meowth.y = 0;
        meowth.x = Math.random() * 1000;
    }
    ctx.drawImage(meowth.bild, meowth.x, meowth.y);
}
    
// Animationsloopen
function loopen() {
    // Sudda ut hela canvas
    ctx.clearRect(0, 0, 1200, 1000);

    // Rita ut figurerna
    ritaPidgey();
    ritaPsyduck();
    ritaDesura();
    ritaEevee();
    ritaMeowth();

    requestAnimationFrame(loopen);
}
```
