# Spelet fånga monstret

## Resultat

![](<../.gitbook/assets/image (5).png>)

## Startkoden

{% tabs %}
{% tab title="monster.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Fånga monstret</title>
    <link rel="stylesheet" href="./monster.css">
</head>
<body>
    <canvas></canvas>
    <script src="./monster.js"></script>
</body>
</html>
```
{% endtab %}

{% tab title="monster.css" %}
```css
body {
    background: #F9F6EB;
}
canvas {
    margin: 20px auto;
    display: block;
    background: url("./images/background.png");
}
```
{% endtab %}
{% endtabs %}

## Mallen

```javascript
// Element vi jobbar med
const eCanvas = document.querySelector("canvas");

// Skapa canvas
var ctx = eCanvas.getContext("2d");
eCanvas.width = 512;
eCanvas.height = 480;

// Objekten i spelet
var spel = {
    tid: 0,
    poäng: 0,
    isGameOver: false,
    bild: new Image()
};
var hjälte = {
    x: 0,
    y: 0,
    a: 5,
    bild: new Image()
};
var monster = {
    x: 0,
    y: 0,
    bild: new Image()
};

// Ladda in bilderna

// Canvas inställningar

// Kör igång spelet
gameLoop();

/* ************ */
/* Funktionerna */
/* ************ */

// Återställ spelet
// Placera ut hjälten
// Spawna monstret slumpmässigt
function reset() {
}

// Kollar om hjälten träffar monstret
function kollaKollision() {
}

// Ritar ut alla delar

// Spelloopen
function gameLoop() {
    ritaBakgrund();
    ritaHjälte();
    ritaMonster();
    kollaKollision();
    
    requestAnimationFrame(gameLoop);
}

/* ********* */
/* Händelser */
/* ********* */

/* Lyssna på tangentnedtryckningar */
window.addEventListener("keydown", function(e) {

});
```

## Grafiken

```javascript
// Ladda in bilderna
spel.bild.src = "images/background.png";
hjälte.bild.src = "images/hero.png";
monster.bild.src = "images/monster.png";
​
// Canvas inställningar
ctx.fillStyle = "#FFF";
```

## Rita figurer

```javascript
function ritaHjälte() {
    ctx.drawImage(hjälte.bild, hjälte.x, hjälte.y);
}
function ritaMonster() {
    ctx.drawImage(monster.bild, monster.x, monster.y);
}
```

## Interaktivtet

```javascript
window.addEventListener("keydown", function(e) {
    switch (e.key) {
        case "ArrowRight":
            if (hjälte.x < eCanvas.width - 60) {
                hjälte.x += hjälte.a;
            }
            break;
        case "ArrowLeft":
            if (hjälte.x > 30) {
                hjälte.x -= hjälte.a;
            }
            break;
        case "ArrowDown":
            if (hjälte.y < eCanvas.height - 60) {
                hjälte.y += hjälte.a;
            }
            break;
        case "ArrowUp":
            if (hjälte.y > 30) {
                hjälte.y -= hjälte.a;
            }
    }
});
```

## Återställa spelet

```javascript
function reset() {
    hjälte.x = eCanvas.width / 2;
    hjälte.y = eCanvas.height / 2;

    /* Spawna monster någonstans på spelplanen */
    monster.x = 32 + (Math.random() * (eCanvas.width - 100));
    monster.y = 32 + (Math.random() * (eCanvas.height - 100));

    /* Starta timern */
    spel.tid = 60;
}
```

## Poäng

```javascript
function kollaKollision() {

    // Träffar dom varandra?
    if (hjälte.x <= (monster.x + 32) && monster.x <= (hjälte.x + 32) && 
        hjälte.y <= (monster.y + 32) && monster.y <= (hjälte.y + 32)) {
        spel.poäng++;
        reset();
    }

    ctx.font = "24px Helvetica";
    ctx.fillText("Fångade monster: " + spel.poäng, 32, 50);
}
```
