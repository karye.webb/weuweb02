Redigera och förbättra kurssidan nedan under ***-strecket.
Fyll på med förklarande text för att göra sidan mer pedagogisk.
Dela upp i steg-för-steg instruktioner.
Lägg till om något eventuellt saknas.
Förklara alla termer och begrepp.
Skriv ut alla kodlistningarna hela i svaret.

*******************************************