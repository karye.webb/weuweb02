# OOP och klasser

```javascript
/*****************************************/
/*             Inställningar             */
/*****************************************/
// Hitta element på sidan
const canvas = document.querySelector("canvas");
const ePoints = document.querySelector("p");

// Ställ in storlek på canvas
canvas.width = 800;
canvas.height = 600;

// Starta canvas rit-api
var ctx = canvas.getContext("2d");

/*****************************************/
/*          Globala variabler            */
/*****************************************/
// Skapa labyrinten
var karta = [
    [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], // rad 0
    [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], // rad 1
    [1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1], // rad 2
    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1], // rad 3
    [1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1], // ...
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1],
    [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

/*****************************************/
/*          Objekten som syns            */
/*****************************************/
// Spelaren
var figur = {
    rad: 0,
    kolumn: 0,
    rotation: 0,
    poäng: 0,
    bild: new Image(),
    rita() {
        ctx.save();
        ctx.translate(this.kolumn * 50 + 25, this.rad * 50 + 25);
        ctx.rotate(this.rotation / 180 * Math.PI);
        ctx.drawImage(this.bild, -25, -25, 50, 50);
        ctx.restore();
    },
    plockaPoäng(mynt) {
        // Är figuren är i samma ruta som myntet?
        if (this.rad == mynt.rad && this.kolumn == mynt.kolumn) {
            // Öka poäng
            this.poäng++;
            ePoints.textContent = this.poäng;
    
            // Spawna om myntet
            mynt.spawna();
        }
    }
}
figur.bild.src = "../bilder/nyckelpiga.png";

// Monstret
var monster = {
    rad: 0,
    kolumn: 0,
    rotation: 0,
    poäng: 0,
    bild: new Image(),
    rita() {
        ctx.save();
        ctx.translate(this.kolumn * 50 + 25, this.rad * 50 + 25);
        ctx.rotate(this.rotation / 180 * Math.PI);
        ctx.drawImage(this.bild, -25, -25, 50, 50);
        ctx.restore();
    },
    spawna() {
        // Oändlig loop
        while (true) {
            this.rad = Math.floor(Math.random() * 12); // 0, 1, 2, 3 .. 11
            this.kolumn = Math.floor(Math.random() * 16); // 0, 1, 2 .. 15
    
            // Avbryt när objektet hamnar på 0
            if (karta[this.rad][this.kolumn] == 0) {
                break;
            }
        }
    }
}
monster.bild.src = "../bilder/monster.png";

// Mynt objekten
var mynt1 = {
    rad: 0,
    kolumn: 0,
    bild: new Image(),
    rita() {
        ctx.drawImage(this.bild, this.kolumn * 50, this.rad * 50, 50, 50);
    },
    spawna() {
        // Oändlig loop
        while (true) {
            this.rad = Math.floor(Math.random() * 12); // 0, 1, 2, 3 .. 11
            this.kolumn = Math.floor(Math.random() * 16); // 0, 1, 2 .. 15
    
            // Avbryt när objektet hamnar på 0
            if (karta[this.rad][this.kolumn] == 0) {
                break;
            }
        }
    }
}
mynt1.bild.src = "../bilder/coin.png";

var mynt2 = {
    rad: 0,
    kolumn: 0,
    bild: new Image(),
    rita() {
        ctx.drawImage(this.bild, this.kolumn * 50, this.rad * 50, 50, 50);
    },
    spawna() {
        // Oändlig loop
        while (true) {
            this.rad = Math.floor(Math.random() * 12); // 0, 1, 2, 3 .. 11
            this.kolumn = Math.floor(Math.random() * 16); // 0, 1, 2 .. 15
    
            // Avbryt när objektet hamnar på 0
            if (karta[this.rad][this.kolumn] == 0) {
                break;
            }
        }
    }
}
mynt2.bild.src = "../bilder/coin.png";

/*****************************************/
/*  Kod som körd innan loopen startar    */
/*****************************************/
monster.spawna();
mynt1.spawna();
mynt2.spawna();

/*****************************************/
/*          Animationsloopen             */
/*****************************************/
function loopen() {
    ctx.clearRect(0, 0, 800, 600);

    ritaKartan();

    figur.rita();
    monster.rita();

    mynt1.rita();
    mynt2.rita();

    figur.plockaPoäng(mynt1);
    figur.plockaPoäng(mynt2);
    
    requestAnimationFrame(loopen);
}
loopen();

/*****************************************/
/*              Funktioner               */
/*****************************************/
// Rita kartan
function ritaKartan() {
    // Loopa igenom raderna
    for (var rad = 0; rad < 12; rad++) {

        // Loopa igenom kolumnerna
        for (var kolumn = 0; kolumn < 16; kolumn++) {

            // Om "1" rita ut en kloss (vägg)
            if (karta[rad][kolumn] == 1) {
                ctx.fillRect(kolumn * 50, rad * 50, 50, 50);
            }
        }
    }
}

/*****************************************/
/*             Interaktion               */
/*****************************************/
// Lyssna på piltangenter
window.addEventListener("keypress", function(e) {
    switch (e.code) {
        case "Numpad2": // Pil nedåt
            // Är det 0 (gång) i rutan nedanför?
            if (karta[figur.rad + 1][figur.kolumn] == 0) {
                // Isåfall flytta dit
                figur.rad++;
            }
            figur.rotation = 180;
            break;
        case "Numpad8": // Pil uppåt
            // Är det 0 i rutan ovanför?
            if (karta[figur.rad - 1][figur.kolumn] == 0) {
                // Isåfall flytta dit
                figur.rad--;
            }
            figur.rotation = 0;
            break;
        case "Numpad4": // Pil vänster
            if (karta[figur.rad][figur.kolumn - 1] == 0) {
                figur.kolumn--;
            }
            figur.rotation = 270;
            break;
        case "Numpad6": // Pil höger
            if (karta[figur.rad][figur.kolumn + 1] == 0) {
                figur.kolumn++;
            }
            figur.rotation = 90;
            break;

        default:
            break;
    }
    //console.log("Kolumn: " + figur.kolumn + ", rad: " + figur.rad);
})
```
