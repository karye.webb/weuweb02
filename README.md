---
description: Introduktion till kursen webbutveckling 2
---

# Introduktion

## Centralt innehåll

Undervisningen i kursen ska behandla följande centrala innehåll:

* **Webben som plattform för applikationer av olika slag.**
* **Utvecklingsprocessen för ett webbtekniskt projekt med målsättningar, planering, specifikation av struktur och design, kodning, optimering, testning, dokumentation och uppföljning.**
* **Fördjupning i märkspråk där det huvudsakliga innehållet är standarderna för HTML och CSS med särskilt fokus på responsiv design.**
* **Språk med stöd för variabler för att förenkla CSS-generering.**
* **Ramverk eller klassbibliotek inom design eller skriptspråk.**
* **Skriptspråk för webbutveckling på klientsidan och dokumentobjektsmodell.**
* **Bilder, ljud, video och två- eller tredimensionell interaktiv grafik för webbapplikationer.**
* Riktlinjer för god praxis inom webbutveckling.
* Uppnående av interoperabilitet genom att följa standarder och testa på olika plattformar.
* Applikationer som fungerar oberoende av vald plattform och hur tillgänglighet uppnås även för användare med funktionsnedsättning.
* Kvalitetssäkring av applikationens funktion och validering av kodens kvalitet.
* Lagar och andra bestämmelser som styr digital information, till exempel personuppgiftslagen och lagen om elektronisk kommunikation.
* Säkerhet och sätt att identifiera hot och sårbarheter samt hur attacker kan motverkas genom effektiva åtgärder.
* **Terminologi inom området webbutveckling.**

## Bra webbsidor för att fördjupa dig i webbutveckling

* [http://pragmatisk.se](http://pragmatisk.se)
* [https://learn-the-web.algonquindesign.ca/courses/web-design-1](https://learn-the-web.algonquindesign.ca/courses/web-design-1/)
* [https://pixlr.com/se/x/](https://pixlr.com/se/x/)
