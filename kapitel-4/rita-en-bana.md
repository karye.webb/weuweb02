# Rita en bana

## Resultat

![](<../.gitbook/assets/image (8).png>)

## Skapa en bana

```javascript
// Element vi arbetar med
const eCanvas = document.querySelector("canvas");

// Ställ in bredd och storlek
eCanvas.width = 800;
eCanvas.height = 600;

// Starta canvas rityta
var ctx = eCanvas.getContext("2d");

// Globala variabler
var piga = {
    x: 0,
    y: 0,
    rot: 0,
    bild: new Image()
}
var karta = [
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
];

// Nyckelpigans startläge
piga.x = 350;
piga.y = 250;
piga.bild.src = "../bilder/nyckelpiga.png";

// Rita ut nyckelpigan
function ritaPiga() {
    ctx.save();
    ctx.translate(piga.x, piga.y);
    ctx.rotate(piga.rot);
    ctx.drawImage(piga.bild, -25, -25, 50, 50);
    ctx.restore();
}

// Rita ut kartan
function ritaKarta() {
    for (var rad = 0; rad < karta.length; rad++) {
        for (var kol = 0; kol < karta[rad].length; kol++) {
            if (karta[rad][kol] == 1) {
                ctx.fillRect(kol * 50, rad * 50, 50, 50);
            }
        }
    }
}

// Lyssna på pil-tangenter
window.addEventListener("keydown", function(e) {
    switch (e.key) {
        case "ArrowRight":
            piga.x += 5;
            piga.rot = 90 * (Math.PI / 180);
            if (piga.x > eCanvas.width) {
                piga.x = 0;
            }
            break;
        case "ArrowLeft":
            piga.x -= 5;
            piga.rot = 270 * (Math.PI / 180);
            if (piga.x < 0) {
                piga.x = c.width;
            }
            break;
        case "ArrowDown":
            piga.y += 5;
            piga.rot = Math.PI;
            if (piga.y > eCanvas.height) {
                piga.y = 0;
            }
            break;
        case "ArrowUp":
            piga.y -= 5;
            piga.rot = 0;
            if (piga.y < 0) {
                piga.y = eCanvas.height;
            }
    }
});

// Animationsloopen
function gameLoop() {
    // Rensa canvas
    ctx.clearRect(0, 0, eCanvas.width, eCanvas.height);

    ritaKarta();
    ritaPiga();

    requestAnimationFrame(gameLoop);
}

// Starta spelet
gameLoop();
```
