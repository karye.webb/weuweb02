# Intro till canvas

## Resultat

![Två rutor ritade på canvas](<../.gitbook/assets/image (23).png>)

## Startkod

* Ladda ned bakgrundsbilden som [grid.png](https://en.wikipedia.org/wiki/File:Locator\_Grid.png)

{% tabs %}
{% tab title="canvas.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Canvas</title>
    <link rel="stylesheet" href="../canvas.css">
</head>
<body>
    <canvas></canvas>
    <script>
        // Element vi arbetar med
        const eCanvas = document.querySelector("canvas");

        // Ställ in bredd och storlek = fullscreen
        eCanvas.width = 600;
        eCanvas.height = 500;

        // Väljer rit-api
        var ctx = eCanvas.getContext("2d");

        // Rita en grön ruta
        ctx.fillStyle = "green";
        ctx.fillRect(10, 10, 100, 100);

        // Rita en röd ruta
        ctx.strokeStyle = "red";
        ctx.strokeRect(200, 10, 100, 100);

    </script>
</body>
</html>
```
{% endtab %}

{% tab title="canvas.css" %}
```css
body {
    background: #F9F6EB;
}
canvas {
    background: #FFF url("./bilder/grid.png");
    border: 1px solid #CCC;
    width: 700px;
}
```
{% endtab %}
{% endtabs %}
