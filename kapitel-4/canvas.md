---
description: Hur man ritar ut en bild på canvas
---

# Rita en bild

## Resultat

![En bild utritad på canvas](<../.gitbook/assets/image (21).png>)

## Startkod

### Infoga en figur på canvas

* Ladda ned bakgrundsbilden som [grid.png](https://en.wikipedia.org/wiki/File:Locator\_Grid.png)
* Ladda ned figuren:

![nyckelpiga.png](../.gitbook/assets/image.png)

{% tabs %}
{% tab title="nyckelpiga.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Canvas</title>
    <link rel="stylesheet" href="../nyckelpiga.css">
</head>
<body>
    <canvas></canvas>
    <script>
        // Element vi arbetar med
        const eCanvas = document.querySelector("canvas");

        // Ställ in bredd och storlek = fullscreen
        eCanvas.width = 600;
        eCanvas.height = 500;

        // Väljer rit-api
        var ctx = eCanvas.getContext("2d");

        // Figuren
        var piga = new Image();
        piga.src = "../bilder/nyckelpiga.png";

        piga.addEventListener("load", function() {
            ctx.drawImage(piga, 100 - 50/2, 100 - 50/2, 50, 50);
        });

    </script>
</body>
</html>
```
{% endtab %}

{% tab title="nyckelpiga.css" %}
```css
body {
    background: #F9F6EB;
}
.kontainer {
    width: 900px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
}
.kol2 {
    display: grid;
    grid-template-columns: auto auto;
    gap: 1em;
}
canvas {
    background: #FFF url("./bilder/grid.png");
    border: 1px solid #CCC;
    width: 700px;
}
button {
    margin: 3em 0;
}

.kolumn {
    padding: 20px;
}
h4 {
    margin-top: 20px;
}
input {
    width: 100%;
    margin: 5px 0;
    margin-bottom: 20px;
}
table {
    width: 100%;
}

```
{% endtab %}
{% endtabs %}

## Animationsloopen

```javascript
// Element vi arbetar med
const eCanvas = document.querySelector("canvas");

// Ställ in bredd och storlek = fullscreen
eCanvas.width = 600;
eCanvas.height = 500;

// Väljer rit-api
var ctx = eCanvas.getContext("2d");

// Figuren
var piga = new Image();
piga.src = "../bilder/nyckelpiga.png";

// Här lägger vi allt som animeras
function gameLoop() {
    ctx.drawImage(piga, 100 - 50/2, 100 - 50/2, 50, 50);

    // Upprepa loopen
    window.requestAnimationFrame(gameLoop);
}

// Kör igång loopen
gameLoop();
```
