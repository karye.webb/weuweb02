# Styra med piltangenter

##

```javascript
// Element vi arbetar med
const eCanvas = document.querySelector("canvas");

// Ställ in bredd och storlek
eCanvas.width = 800;
eCanvas.height = 600;

// Ritytan
ctx = eCanvas.getContext("2d");

// Globala varariabler
var piga = {
    x: 0,
    y: 0,
    rot: 0,
    vänster: false,
    höger: false,
    upp: false,
    ned: false,
    bild: new Image()
};

// Figurens ursprungsläge
piga.x = eCanvas.width / 2;
piga.y = eCanvas.height / 2;
piga.bild.src = "../bilder/nyckelpiga.png";

function ritaPiga() {
    ctx.save();
    ctx.translate(piga.x, piga.y);
    ctx.rotate(piga.rot);
    ctx.drawImage(piga.bild, -25, -25, 50, 50);
    ctx.restore();
}

// Lyssna på tangenter
window.addEventListener("keydown", function(e) {
    switch (e.key) {
        case "ArrowUp":
            piga.upp = true;
            break;
        case "ArrowDown":
            piga.ned = true;
            break;
        case "ArrowLeft":
            piga.vänster = true;
            break;
        case "ArrowRight":
            piga.höger = true;
            break;
    }
});
window.addEventListener("keyup", function(e) {
    switch (e.key) {
        case "ArrowUp":
            piga.upp = false;
            break;
        case "ArrowDown":
            piga.ned = false;
            break;
        case "ArrowLeft":
            piga.vänster = false;
            break;
        case "ArrowRight":
            piga.höger = false;
            break;
    }
});

function uppdateraPiga() {
    if (piga.vänster) {
        piga.x -= 5;
        piga.rot = (270 * Math.PI) / 180;
        if (piga.x < 0) {
            piga.x = c.width;
        }
    } else
    if (piga.höger) {
        piga.x += 5;
        piga.rot = (90 * Math.PI) / 180;
        if (piga.x > eCanvas.width) {
            piga.x = 0;
        }
    } else
    if (piga.upp) {
        piga.y -= 5;
        piga.rot = 0;
        if (piga.y < 0) {
            piga.y = eCanvas.height;
        }
    } else
    if (piga.ned) {
        piga.y += 5;
        piga.rot = Math.PI;
        if (piga.y > eCanvas.height) {
            piga.y = 0;
        }
    }
}

function gameLoop() {
    // Sudda bort allt
    ctx.clearRect(0, 0, eCanvas.width, eCanvas.height);

    uppdateraPiga();
    ritaPiga();

    requestAnimationFrame(gameLoop);
}

// Starta spelet
gameLoop();
```

##
