# Infoga Mapbox-karta

## Resultat

![](<../.gitbook/assets/image (11).png>)

## Startkod

{% tabs %}
{% tab title="mapbox1.html" %}
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset='utf-8'>
    <title>Display a map</title>
    <meta name='viewport' content='initial-scale=1, maximum-scale=1, user-scalable=no'>
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="mapbox.css">
</head>
<body>
    <div id='map'></div>
    <script>
        mapboxgl.accessToken = 'pk.eyJ1Ijoia2FyeWUiLCJhIjoiY2pwOXRtbWc1MGdmNjNwc2JmdGxzeDR5byJ9.whp8f2Ttws57ctAf_stuag';
        var map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/karye/ckm1w3jpbalnn17ptoqwv7358', // stylesheet location
            center: [18.07, 59.33], // starting position [lng, lat]
            zoom: 10 // starting zoom
        });

        var marker1 = new mapboxgl.Marker()
            .setLngLat([18.0, 59.37])
            .addTo(map);
        var marker2 = new mapboxgl.Marker({ draggable: true })
            .setLngLat([17.9, 59.35])
            .addTo(map);
        var marker3 = new mapboxgl.Marker({ draggable: true })
            .setLngLat([18.3, 59.33])
            .addTo(map);
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="mapbox.css" %}
```css
body {
    margin: 0;
    padding: 0;
}
.kontainer {
    width: 1200px;
    margin: 20px auto;
    display: grid;
    grid-template-columns: 1fr 1fr;
    gap: 1em;
}
#map {
    width: 100%; 
    height: 800px;
}
.box {
    background: #000000a9;
    color: #FFF;
    padding: 20px;
    border-radius: 5px;
}
p {
    margin: 1em;
}
```
{% endtab %}
{% endtabs %}

## Samla positioner

![](<../.gitbook/assets/image (12).png>)

### Frontend

```javascript
// Hitta tabellen
const eTable = document.querySelector("table");
const eKnapp = document.querySelector("button");
const eSvar = document.querySelector("p");

// Min personliga access-token
mapboxgl.accessToken = "pk.eyJ1Ijoia2FyeWUiLCJhIjoiY2pwOXRtbWc1MGdmNjNwc2JmdGxzeDR5byJ9.whp8f2Ttws57ctAf_stuag";

// Skapa kartan
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/karye/ckm1xg4enamz217pt54o72zij',
    center: [18.071723, 59.338477], // Longitude, Latitude
    zoom: 11
});

// Lägga till markers när man klickar på kartan
map.on("click", function (e) {
    console.log("Du har klickat på kartan!", e.lngLat);

    // Infoga en marker
    var marker = new mapboxgl.Marker()
        .setLngLat(e.lngLat)
        .addTo(map);

    // Infoga rad i tabellen
    var newRow = eTable.insertRow();

    // Infogas första cellen
    // .. och skriver in latitude-texten
    newRow.insertCell().innerText = e.lngLat.lng;

    // Infogas andra cellen
    // .. och skriver in longituden
    newRow.insertCell().innerText = e.lngLat.lat;

    // Infogas tredje cellen
    // .. gör den redigerbar
    // .. skiver in en exempeltext
    var lastCell = newRow.insertCell();
    lastCell.contentEditable = "true";
    lastCell.innerText = "...";
});

// Klick på knappen läser in alla koordinater från tabellen
eKnapp.addEventListener("click", function () {
    // Skriv ut innehållet av tabellen i loggen
    // 1. Hitta första cellen
    const eCell = document.querySelector("td");
    // 2. Läs av innehållet
    console.log(eCell.textContent);

    // Hitta ALLA celler
    const eCeller = document.querySelectorAll("td");

    // Loopa igenom alla celler
    var allaRader;
    eCeller.forEach(cell => {
        console.log(cell.innerText);
        allaRader += cell.innerText;
    });

    // Skicka data
    // Låtsas att vi har ett formulär
    var formData = new FormData();
    formData.append("texten", allaRader);

    // Skicka till Backend
    fetch("spara3.php", {                    // Skickar!
        method: "post",
        body: formData
    })
        .then(response => response.text())  // Tar emot svar
        .then(svar => {
            if (svar == "Sparad") {
                eSvar.textContent = "Alla markers har sparats";

                // Se https://devdocs.io/dom/element/classlist
                eSvar.classList.add("alert", "alert-success");
            } else {
                eSvar.textContent = "Något blev fel, markers har inte sparats";
                eSvar.classList.add("alert", "alert-warning"); 
            }
        })
})
```

### Backend

```php
<?php
// Ta emot text
$texten = filter_input(INPUT_POST, "texten");

if ($texten) {
    // Öppna textfil för att skriva i
    $handtag = fopen("markers.tsv", "w");

    // Skriv en rad
    fwrite($handtag, $texten);

    // Stäng ned filen
    fclose($handtag);

    echo "Sparad";
} else {
    echo "Något blev fel";
}

```
