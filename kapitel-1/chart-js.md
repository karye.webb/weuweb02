---
description: Hämta temperaturprognosen från SMHI och rita ut den
---

# Temperaturgraf med Chart.js

## Resultat

![](<../.gitbook/assets/smhi-chart.png>)

## Del 1 - rita en graf

### Biblioteket [chartjs.org](https://www.chartjs.org)

* Provar att rita ut temperaturkurva som på [smhi.se](https://www.smhi.se/vader/prognoser/ortsprognoser/q/N%C3%A4lsta/Stockholm/2690494#tab=chart)

![](<../.gitbook/assets/smhi.png>)

* För att rita ut en kurva måste man ange serier med data:

{% tabs %}
{% tab title="graf.html" %}
```javascript
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Testar Chart.js</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <link rel="stylesheet" href="./prognos.css">
</head>
<body>
    <div class="kontainer">
        <canvas id="myChart" width="400" height="100"></canvas>
    </div>
    <script>
        const eCanvas = document.querySelector('#myChart');
        var ctx = eCanvas.getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    '2020-04-27',
                    '',
                    '',
                    '',
                    '',
                    '2020-04-28',
                    '',
                    '',
                    '',
                    '',
                    '2020-04-29',
                ],
                datasets: [{
                    label: 'SMHI prognos',
                    data: [
                        6.2,
                        6.8,
                        7,
                        7.3,
                        7.3,
                        7.3,
                        6.2,
                        6.8,
                        7,
                        7.3,
                        7.3,
                        7.3
                    ],
                    backgroundColor: [
                        'rgb(173, 216, 230, 0.3)'
                    ],
                    borderColor: [
                        'blue'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Noto+Serif|Pacifico&display=swap');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}

body {
    background: #F9F6EB;
}
.kontainer {
    width: 1200px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Source Sans Pro', sans-serif;
    border: 2px solid #a0a0a0;
    color: #4e4e4e;
}
footer {
    background: #9b9b9b;
    margin-top: 1em;
    padding: 1em;
    border-radius: 5px;
    color: #FFF;
}
footer img {
    width: 50px;
}
```
{% endtab %}
{% endtabs %}

## Del 2 - SMHI:s api

* Först skickar man ett anrop till api med koordinater
* .. och får JSON som svar

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Temperaturprognos från SMHI api</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <link rel="stylesheet" href="./prognos.css">
</head>
<body>
    <div class="kontainer">
        <h1>Temperaturprognos - Stockholm</h1>
        <canvas id="myChart" width="400" height="100"></canvas>
        <?php
        // Url till api:et
        $url = "https://opendata-download-metfcst.smhi.se/api/category/pmp3g/version/2/geotype/point/lon/18/lat/59/data.json";

        // Hämta JSON
        $json = file_get_contents($url);

        // Avkoda JSON
        $jsonData = json_decode($json);
        ?>
        <footer>
        <p><img src="https://www.smhi.se/polopoly_fs/1.117503.1490015865!/image/smhi-logo-120.png_gen/derivatives/Original/image/smhi-logo-120.png" alt="SMHI"> Prognos från SMHI api. <img src="https://www.chartjs.org/img/chartjs-logo.svg" alt="https://www.chartjs.org/"> Graf skapad med chartjs.org</p>
        </footer>
    </div>
</body>
</html>
```

* .. sedan plockar vi ut tider och temperaturer

```php
// Plocka ut publiceringsdatum
$approvedTime = $jsonData->approvedTime;
echo "<p>Prognos publicerad den $approvedTime</p>";

// Plocka ut tidserien
$timeSeries = $jsonData->timeSeries;

// Samla in datat: tider och temperaturer
$tiderData = "";
$tempData = "";

// Bara ta med 55 första värdena 
$timeSeries = array_slice($timeSeries, 0, 55);
```

* .. därefter loopar man igenom och samlar ihop till serier som kan användas i chartjs

```php
// Loopa arrayen för att plocka ut temperaturvärdena
foreach ($timeSeries as $timeData) {
    // Plocka tidpunkt
    $validTime = $timeData->validTime;

    // Plocka ut alla parametrar
    $parameters = $timeData->parameters;

    // Plocka ut temperaturen = objekt nr 12
    $parameter = $parameters[11];
    $temperaturen = $parameter->values[0];

    // Plocka bara datum-delen: substr()
    $datumDelen = substr($validTime, 0, 10);

    // För att bara skriva datumet en första gång
    $pos = strpos($tiderData, $datumDelen);

    if ($pos === false) {
        $tiderData .= "'$datumDelen', ";
    } else {
        $tiderData .= "'', ";
    }

    $tempData .= "'$temperaturen', ";
}

// Chart.js koden
echo "<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [$tiderData],
        datasets: [{
            label: 'SMHI prognos',
            data: [$tempData],
            backgroundColor: [
                'rgb(173, 216, 230, 0.3)'
            ],
            borderColor: [
                'blue'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>";
```
