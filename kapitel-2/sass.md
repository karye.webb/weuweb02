# Enklare CSS med Sass

## Intro till Sass

{% embed url="https://youtu.be/Zz6eOVaaelI" %}

## Ställ in VS Code

### Installera tillägg

* [https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
* [https://marketplace.visualstudio.com/items?itemName=Syler.sass-indented](https://marketplace.visualstudio.com/items?itemName=Syler.sass-indented)

## Enkel varukorg

### Resultat

![](<../.gitbook/assets/image (19).png>)

### Startkod

{% tabs %}
{% tab title="varukorg.html" %}
```html
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Varukorg</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="./style/varukorg.css">
</head>
<body>
    <div class="kontainer">
        <h1>Varukorg</h1>
        <div class="kol-4">
            <h3>Produkt</h3>
            <h3>Pris</h3>
            <h3>Antal</h3>
            <h3>Summa</h3>

            <p class="vara"><img src="https://www.electrokit.com/uploads/productimage/41018/41018074-1-300x225.jpg" alt="Raspberry Pi Pico"> Raspberry Pi Pico</p>
            <p class="pris">49</p>
            <p class="kol-3">
                <button class="minus"><i class="bi bi-dash-circle"></i></button>
                <input class="antal" type="text">
                <button class="plus"><i class="bi bi-plus-circle"></i></button>
            </p>
            <p class="summa"></p>
        </div>
    </div>
    <script src="varukorg.js"></script>
</body>
</html>
```
{% endtab %}

{% tab title="style.sass" %}
```sass
@import url('https://fonts.googleapis.com/css?family=Open+Sans')

body
    background: #F9F6EB

.kol-4
    margin: 1em 0
    display: grid
    grid-template-columns: 2fr 1fr 2fr 1fr
    align-items: center

.kontainer
    width: 700px
    padding: 2em
    margin: 3em auto
    background: #fff
    border-radius: 5px
    font-family: 'Open Sans', sans-serif
    border: 1px solid #ddd
    box-shadow: 0 0 12px #f0e9d1

    label
        display: grid
        grid-template-columns: 1fr 2fr
        margin: 10px 0
        padding: 0

    input, textarea
        padding: 0.5em
        border-radius: 0.3em
        border: 1px solid #ccc
        font-weight: bold
        box-shadow: inset 0 2px 2px #0000001a

    textarea
        height: 10em
        width: 100%

    button
        padding: 0.7em
        border-radius: 0.3em
        border: none
        font-weight: bold
        color: #FFF
        background-color: #55a5d2
        cursor: pointer

    input
        width: 3em

    img
        width: 2em
```
{% endtab %}

{% tab title="varukorg.js" %}
```javascript
// Det som händer när program startar
// Element vi skall använda
const rutaPris = document.querySelector(".pris");
const knappPlus = document.querySelector(".plus");
const knappMinus = document.querySelector(".minus");
const rutaAntal = document.querySelector(".antal");
const rutaSumma = document.querySelector(".summa");

// Tre globala variabler för att hålla koll pris, antal, summa
var pris = rutaPris.textContent;
var antal = 1;
var summa = pris * antal;
console.log(pris, antal, summa);

// Skriv ut i sidan
rutaAntal.value = antal;
rutaSumma.textContent = summa;

// Det som händer när man klickar på minus
knappMinus.addEventListener("click", function () {
    if (antal > 0) {
        antal--;
        console.log(antal);
        rutaAntal.value = antal;
        rutaSumma.textContent = pris * antal;
    }
})

// Det som händer när man klickar på plus
knappPlus.addEventListener("click", function () {
    antal++;
    console.log(antal);
    rutaAntal.value = antal;
    rutaSumma.textContent = pris * antal;
})

// Det som händer när man skriver i antal-rutan
rutaAntal.addEventListener("input", function () {
    antal = rutaAntal.value;
    rutaSumma.textContent = pris * antal;
})


```
{% endtab %}
{% endtabs %}

### Mappstrukturen

![](<../.gitbook/assets/image (17).png>)

## Kompilering från Sass till CSS

* När man aktiverat Live Sass Compiler skapas automatiskt style.css

![](<../.gitbook/assets/image (18).png>)
