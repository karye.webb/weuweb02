#!/usr/bin/env python3
# To do bulk deletions, you can use the pipelines API to programmatically list and delete pipelines.
# In Python (with the python-gitlab library)

import gitlab

project_id = 30582577

gl = gitlab.Gitlab('https://gitlab.com', private_token='glpat-Z-ax1W9FPxb5h3qtHAA8')
gl.auth()

# Get all pipelines for a project
pipelines = gl.projects.get(project_id).pipelines.list(get_all=True)

# Loop through the pipelines and delete them (except the first 5)
for pipeline in pipelines[5:]:
    # Print the pipeline ID and status
    print(pipeline.id, pipeline.status)
    # Delete the pipeline
    pipeline.delete()