# Table of contents

* [Introduktion](README.md)
* [Utvecklingsmiljö](utvecklingsmiljoe.md)

## Använda bibliotek <a href="kapitel-1" id="kapitel-1"></a>

* [Modal-fönster med Bootstrap](kapitel-1/bootstrap-modal-foenster.md)
* [SMHI graf med Chart.js](kapitel-1/chart-js.md)
* [Infoga Mapbox-karta](kapitel-1/mapbox-karta.md)
* [Andra bibliotek](kapitel-1/andra-bibliotek.md)

## CSS-generering med SASS <a href="kapitel-2" id="kapitel-2"></a>

* [Enklare CSS med Sass](kapitel-2/sass.md)

## Uppdatera DOM med skript <a href="kapitel-3" id="kapitel-3"></a>

* [Ladda flaggor dynamiskt](kapitel-3/dynamisk-ladda-flaggor.md)

## 2D animationer och interaktivitet 1:3 <a href="kapitel-4" id="kapitel-4"></a>

* [Intro till canvas](kapitel-4/intro-till-canvas.md)
* [Rita en bild](kapitel-4/canvas.md)
* [Styra med piltangenter](kapitel-4/styra-med-piltangenter.md)
* [Rita en bana](kapitel-4/rita-en-bana.md)
* [Förflytta i ett rutnät](kapitel-4/foerflytta-i-ett-rutnaet.md)

## 2D animationer och interaktivitet 2:3 <a href="kapitel-5" id="kapitel-5"></a>

* [Spelet Invader](kapitel-5/spelet-invader.md)
* [Infoga fler figurer](kapitel-5/infoga-fler-figurer.md)
* [Infoga spelaren](kapitel-5/infoga-spelaren.md)
* [Förenkla koden](kapitel-5/foerenkla-koden.md)

## 2D animationer och interaktivitet 3:3 <a href="kapitel-6" id="kapitel-6"></a>

* [Rita med sprites](kapitel-6/rita-med-sprites.md)
* [Animera med sprite](kapitel-6/animera-med-sprite.md)
* [Förbättrad animation](kapitel-6/foerbaettrad-animation.md)

## Objekt i Javascript <a href="kapitel-7" id="kapitel-7"></a>

* [Objekt och metoder](kapitel-7/objekt-och-metoder.md)
* [OOP och klasser](kapitel-7/oop-och-klasser.md)

## Labbar

* [Spelet fånga monstret](labbar/spelet-fanga-monstret.md)
