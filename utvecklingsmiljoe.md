---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljö

## Installera webbeditorn VS Code & tillägg <a href="installera-webbeditorn-vs-code-and-tillaegg" id="installera-webbeditorn-vs-code-and-tillaegg"></a>

* Installera [VS Code](https://code.visualstudio.com)
* Installera [git-scm](https://git-scm.com)
* Installera tilläggen
  * Beautify
  * Bracket Pair Colorizer 2
  * Live Server
  * Path Intellisense
  * Live Sass Compiler
  * Sass
  * [VSCode Great Icons](https://marketplace.visualstudio.com/items?itemName=emmanuelbeziat.vscode-great-icons)

![](<.gitbook/assets/image (24).png>)

## Skapa en utvecklingsmiljö <a href="skapa-en-utvecklingsmiljoe" id="skapa-en-utvecklingsmiljoe"></a>

* Skapa ett konto på [github.com](https://github.com)
* Skapa en mapp **c:/github/webbutv1**

![](<.gitbook/assets/image (26).png>)

* Publicera mappen på [github.com](https://github.com)

![](<.gitbook/assets/image (27).png>)

## **Webbsnippets**

* Skapa User Snippets för webb

```
{
    "CSS reset": {
        "scope": "css",
        "prefix": "reset",
        "description": "Enkel CSS reset",
        "body": [
            "/* Enkel CSS-reset */",
            "html {",
            "\tbox-sizing: border-box;",
            "}",
            "*, *:before, *:after {",
            "\tbox-sizing: inherit;",
            "}",
            "body, h1, h2, h3, h4, h5, h6, p, ul {",
            "\tmargin: 0;",
            "\tpadding: 0;",
            "}"
        ]
    },
    "HTML5_grundkod": {
        "prefix": "html5",
        "description": "HTML5 grundkod",
        "body": [
            "<!DOCTYPE html>",
            "<html lang=\"sv\">",
            "<head>",
            "\t<meta charset=\"utf-8\">",
            "\t<meta name=\"viewport\" content=\"width=device-width, ,initial-scale=1\">",
            "\t<title></title>",
            "\t<link rel=\"stylesheet\" href=\"style.css\">",
            "</head>",
            "<body>",
            "\t${1}",
            "</body>",
            "</html>"
        ]
    }
}
```
